package com.gitlab.drosseau.testgattserver

import android.app.Service
import android.bluetooth.*
import android.bluetooth.le.AdvertiseCallback
import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertiseSettings
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Binder
import android.os.IBinder
import android.os.ParcelUuid
import android.util.Log
import java.util.*

class GattServerService : Service() {

    private lateinit var mBluetoothManager : BluetoothManager
    private lateinit var mBluetoothServer : BluetoothGattServer

    private val mAdvertiseCallback = AdvCallback()
    private val mCallback = ServerCallback()

    private val mCharHashMap: HashMap<BluetoothGattCharacteristic, ByteArray> = HashMap()
    private val mDescHashMap: HashMap<BluetoothGattDescriptor, ByteArray> = HashMap()

    private val mServices: MutableList<BluetoothGattService> = mutableListOf()
    private var mTotalServices = 0
    private var mServiceIdx = 0

    companion object {
        private const val DEVICE_NAME = "Test"
        private val LOG_TAG = "TestGattServer"

        private const val PREFIX = "com.gitlab.drosseau.testgattserver.GattServerService"
        const val ACTION_START_ADVERTISING = "$PREFIX.ACTION_START_ADVERTISING"
        const val ACTION_STOP_ADVERTISING = "$PREFIX.ACTION_STOP_ADVERTISING"

        private val SERVICE_A = UUID.fromString("f7d1a474-6441-4261-aba7-1209d3794e12")
        private val CHARACTERISTIC_AA = UUID.fromString("0d6b9dc8-6b79-4aed-9f2d-8d3a7df3dc40")
        private val DESCRIPTOR_AAA = UUID.fromString("dba537d9-ea5c-42f3-b4a9-ddff203c2a04")
        private val CHARACTERISTIC_AB = UUID.fromString("defdf8db-48d8-4d82-8183-bbd65cc10fc4")

        private val SERVICE_B = UUID.fromString("17788ff5-1a37-414f-a516-93ddb3abfdd4")
        private val CHARACTERISTIC_BA = UUID.fromString("ba3b33f1-0081-4289-8245-5efe0f56076a")
        private val DESCRIPTOR_BAA = UUID.fromString("ab4b9890-31a9-45e5-b789-85066b0a21d2")
        private val CHARACTERISTIC_BB = UUID.fromString("405f34d1-676d-488a-bb4a-300ce072b087")
    }

    override fun onCreate() {
        super.onCreate()
        setupServer()
        val filter = IntentFilter()
        filter.addAction(ACTION_STOP_ADVERTISING)
        filter.addAction(ACTION_START_ADVERTISING)
        registerReceiver(mReceiver, filter)
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(mReceiver)
    }

    private fun startAdvertising() {
        val adv = mBluetoothManager.adapter.bluetoothLeAdvertiser
        adv?.let {
            val settings = AdvertiseSettings.Builder()
                .setConnectable(true)
                .setTimeout(0)
                .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED)
                .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_MEDIUM)
                .build()
            val data = AdvertiseData.Builder()
                .setIncludeDeviceName(true)
                .setIncludeTxPowerLevel(false)
                .addServiceUuid(ParcelUuid(SERVICE_A))
                .build()

            it.startAdvertising(settings, data, mAdvertiseCallback)
        }
    }

    private fun stopAdvertising() {
        val adv = mBluetoothManager.adapter.bluetoothLeAdvertiser
        adv.stopAdvertising(mAdvertiseCallback)
    }

    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(ctx: Context?, intent: Intent?) {
            intent?.let {
                when (it.action) {
                    ACTION_START_ADVERTISING -> {
                        startAdvertising()
                    }
                    ACTION_STOP_ADVERTISING -> {
                        stopAdvertising()
                    }
                }
            }
        }
    }

    private val mBinder = object : Binder() {
        fun getService() : GattServerService {
            return this@GattServerService
        }
    }

    override fun onBind(intent: Intent): IBinder {
        return mBinder
    }

    private fun setupServer() {
        val serviceA = BluetoothGattService(SERVICE_A, BluetoothGattService.SERVICE_TYPE_PRIMARY)
        val charAA = BluetoothGattCharacteristic(
            CHARACTERISTIC_AA,
            BluetoothGattCharacteristic.PROPERTY_READ or BluetoothGattCharacteristic.PROPERTY_WRITE,
            BluetoothGattCharacteristic.PERMISSION_READ or BluetoothGattCharacteristic.PERMISSION_WRITE
        )
        val charAB = BluetoothGattCharacteristic(
            CHARACTERISTIC_AB,
            BluetoothGattCharacteristic.PROPERTY_READ or BluetoothGattCharacteristic.PROPERTY_WRITE,
            BluetoothGattCharacteristic.PERMISSION_READ or BluetoothGattCharacteristic.PERMISSION_WRITE
        )
        val descAAA = BluetoothGattDescriptor(
            DESCRIPTOR_AAA,
            BluetoothGattDescriptor.PERMISSION_READ or BluetoothGattDescriptor.PERMISSION_WRITE
        )
        Log.d(LOG_TAG, "Char ${charAA.uuid} permissions ${Integer.toBinaryString(charAA.permissions)}")
        Log.d(LOG_TAG, "Char ${charAB.uuid} permissions ${Integer.toBinaryString(charAB.permissions)}")
        Log.d(LOG_TAG, "Desc ${descAAA.uuid} permissions ${Integer.toBinaryString(descAAA.permissions)}")
        charAA.addDescriptor(descAAA)
        serviceA.addCharacteristic(charAA)
        serviceA.addCharacteristic(charAB)
        val serviceB = BluetoothGattService(SERVICE_B, BluetoothGattService.SERVICE_TYPE_PRIMARY)
        val charBA = BluetoothGattCharacteristic(
            CHARACTERISTIC_BA,
            BluetoothGattCharacteristic.PROPERTY_READ or BluetoothGattCharacteristic.PROPERTY_WRITE,
            BluetoothGattCharacteristic.PERMISSION_READ or BluetoothGattCharacteristic.PERMISSION_WRITE
        )
        val charBB = BluetoothGattCharacteristic(
            CHARACTERISTIC_BB,
            BluetoothGattCharacteristic.PROPERTY_READ or BluetoothGattCharacteristic.PROPERTY_WRITE,
            BluetoothGattCharacteristic.PERMISSION_READ or BluetoothGattCharacteristic.PERMISSION_WRITE
        )
        val descBAA = BluetoothGattDescriptor(
            DESCRIPTOR_BAA,
            BluetoothGattDescriptor.PERMISSION_READ or BluetoothGattDescriptor.PERMISSION_WRITE
        )
        Log.d(LOG_TAG, "Char ${charBA.uuid} permissions ${Integer.toBinaryString(charBA.permissions)}")
        Log.d(LOG_TAG, "Char ${charBB.uuid} permissions ${Integer.toBinaryString(charBB.permissions)}")
        Log.d(LOG_TAG, "Desc ${descBAA.uuid} permissions ${Integer.toBinaryString(descBAA.permissions)}")
        charBA.addDescriptor(descBAA)
        serviceB.addCharacteristic(charBA)
        serviceB.addCharacteristic(charBB)
        mBluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        mBluetoothManager.adapter.name = DEVICE_NAME
        mBluetoothServer = mBluetoothManager.openGattServer(this, mCallback)
        mServices.add(serviceA)
        mServices.add(serviceB)
        mTotalServices = mServices.size
        mServiceIdx = 0
        addNextService()

        mCharHashMap[charAA] = "deadbeef".hexToBytes()
        mCharHashMap[charAB] = "facefeed".hexToBytes()
        mCharHashMap[charBA] = "1337c0de".hexToBytes()
        mCharHashMap[charBB] = "cafef00d".hexToBytes()
        mDescHashMap[descAAA] = "badc0de5".hexToBytes()
        mDescHashMap[descBAA] = "faceb00c".hexToBytes()
    }

    private fun addNextService() : Boolean {
        if (mServiceIdx >= mTotalServices) {
            return false
        }
        val service = mServices[mServiceIdx]
        mServiceIdx++
        mBluetoothServer.addService(service)
        return true
    }
    inner class ServerCallback : BluetoothGattServerCallback() {

        override fun onConnectionStateChange(device: BluetoothDevice?, status: Int, newState: Int) {
            super.onConnectionStateChange(device, status, newState)
            when (newState) {
                BluetoothProfile.STATE_CONNECTED -> {
                    Log.i(LOG_TAG, "Device connected: $device")
                }
                BluetoothProfile.STATE_DISCONNECTED -> {
                    Log.i(LOG_TAG, "Device disconnected: $device")
                }
            }
        }

        override fun onCharacteristicReadRequest(device: BluetoothDevice?, requestId: Int, offset: Int, characteristic: BluetoothGattCharacteristic?) {
            super.onCharacteristicReadRequest(device, requestId, offset, characteristic)
            if (characteristic == null) {
                Log.e(LOG_TAG, "Trying to read a null characteristic")
                return
            }
            val byteVal = mCharHashMap[characteristic]
            if (byteVal == null) {
                Log.e(LOG_TAG, "Characteristic ${characteristic.uuid} had a null byte value")
                mBluetoothServer.sendResponse(device, requestId, BluetoothGatt.GATT_FAILURE, 0, null)
                return
            }
            Log.i(LOG_TAG, "Characteristic ${characteristic.uuid} has value: ${byteVal.toHex()}")
            mBluetoothServer.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, byteVal)
        }

        override fun onCharacteristicWriteRequest(device: BluetoothDevice?, requestId: Int, characteristic: BluetoothGattCharacteristic?, preparedWrite: Boolean, responseNeeded: Boolean, offset: Int, value: ByteArray?) {
            super.onCharacteristicWriteRequest(device, requestId, characteristic, preparedWrite, responseNeeded, offset, value)
            var failure = false
            if (value == null) {
                Log.e(LOG_TAG, "Tried to write null value")
                failure = true
            } else if (characteristic == null) {
                Log.e(LOG_TAG,"Tried to write null characteristic")
                failure = true
            } else if (!mCharHashMap.containsKey(characteristic)) {
                Log.e(LOG_TAG, "Don't have characteristic $characteristic")
                failure = true
            }
            if (failure) {
                if (responseNeeded) {
                    mBluetoothServer.sendResponse(device, requestId, BluetoothGatt.GATT_FAILURE, 0, null)
                }
                return
            }
            Log.i(LOG_TAG, "Writing ${value!!.toHex()} to ${characteristic!!.uuid}")
            mCharHashMap[characteristic] = value
            if (responseNeeded) {
                mBluetoothServer.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, null)
            }
        }

        override fun onDescriptorReadRequest(device: BluetoothDevice?, requestId: Int, offset: Int, descriptor: BluetoothGattDescriptor?) {
            super.onDescriptorReadRequest(device, requestId, offset, descriptor)
            if (descriptor == null) {
                Log.e(LOG_TAG, "Tried to read a null descriptor")
                return
            }
            val byteVal = mDescHashMap[descriptor]
            if (byteVal == null) {
                Log.e(LOG_TAG, "Descriptor ${descriptor.uuid} had a null byte value")
                mBluetoothServer.sendResponse(device, requestId, BluetoothGatt.GATT_FAILURE, 0, null)
                return
            }
            Log.i(LOG_TAG, "Descriptor ${descriptor.uuid} has value: ${byteVal.toHex()}")
            mBluetoothServer.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, byteVal)
        }

        override fun onDescriptorWriteRequest(device: BluetoothDevice?, requestId: Int, descriptor: BluetoothGattDescriptor?, preparedWrite: Boolean, responseNeeded: Boolean, offset: Int, value: ByteArray?) {
            super.onDescriptorWriteRequest(device, requestId, descriptor, preparedWrite, responseNeeded, offset, value)
            var failure = false
            if (value == null) {
                Log.e(LOG_TAG, "Tried to write null value")
                failure = true
            } else if (descriptor == null) {
                Log.e(LOG_TAG,"Tried to write null descriptor")
                failure = true
            } else if (!mDescHashMap.containsKey(descriptor)) {
                Log.e(LOG_TAG, "Don't have descriptor ${descriptor.uuid}")
                failure = true
            }
            if (failure) {
                if (responseNeeded) {
                    mBluetoothServer.sendResponse(device, requestId, BluetoothGatt.GATT_FAILURE, 0, null)
                }
                return
            }
            Log.i(LOG_TAG, "Writing ${value!!.toHex()} to ${descriptor!!.uuid}")
            mDescHashMap[descriptor] = value
            if (responseNeeded) {
                mBluetoothServer.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, 0, null)
            }
        }

        override fun onExecuteWrite(device: BluetoothDevice?, requestId: Int, execute: Boolean) {
            super.onExecuteWrite(device, requestId, execute)
        }

        override fun onMtuChanged(device: BluetoothDevice?, mtu: Int) {
            super.onMtuChanged(device, mtu)
        }

        override fun onNotificationSent(device: BluetoothDevice?, status: Int) {
            super.onNotificationSent(device, status)
        }

        override fun onPhyRead(device: BluetoothDevice?, txPhy: Int, rxPhy: Int, status: Int) {
            super.onPhyRead(device, txPhy, rxPhy, status)
        }

        override fun onPhyUpdate(device: BluetoothDevice?, txPhy: Int, rxPhy: Int, status: Int) {
            super.onPhyUpdate(device, txPhy, rxPhy, status)
        }

        override fun onServiceAdded(status: Int, service: BluetoothGattService?) {
            super.onServiceAdded(status, service)
            Log.i(LOG_TAG, "onServiceAdded: status: $status, Service ${service!!.uuid}")
            addNextService()
        }
    }

    inner class AdvCallback : AdvertiseCallback() {
        override fun onStartFailure(errorCode: Int) {
            Log.e(LOG_TAG, "Failed to start advertising: $errorCode")
        }

        override fun onStartSuccess(settingsInEffect: AdvertiseSettings?) {
            Log.i(LOG_TAG, "Successfully started advertising")
        }
    }
}
