package com.gitlab.drosseau.testgattserver

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val intent = Intent(this, GattServerService::class.java)
        startService(intent)

        startAdvertisingBtn.isEnabled = true
        stopAdvertisingBtn.isEnabled = false
        startAdvertisingBtn.setOnClickListener {
            startAdvertising()
            startAdvertisingBtn.isEnabled = false
            stopAdvertisingBtn.isEnabled = true
        }
        stopAdvertisingBtn.setOnClickListener {
            stopAdvertising()
            startAdvertisingBtn.isEnabled = true
            stopAdvertisingBtn.isEnabled = false
        }
    }

    private fun startAdvertising() {
        val intent = Intent(GattServerService.ACTION_START_ADVERTISING)
        intent.setPackage(this.packageName)
        sendBroadcast(intent)
    }

    private fun stopAdvertising() {
        val intent = Intent(GattServerService.ACTION_STOP_ADVERTISING)
        intent.setPackage(this.packageName)
        sendBroadcast(intent)
    }
}
