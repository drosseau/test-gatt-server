package com.gitlab.drosseau.testgattserver

import android.content.Context
import android.widget.Toast

/**
 * Adding a `toast` method to ALL Context's that will show a toast. It will
 * also log the toast message to logcat.
 */
fun Context.toast(msg: CharSequence, len: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, msg, len).show()
}

private val HEX_CHARS = "0123456789abcdef".toCharArray()

fun List<Byte>.toHex() : String {
    if (size == 0) {
        return ""
    }
    val hex = CharArray(size * 2)
    for (i in 0..(size - 1)) {
        val bv = this[i].toInt().and(0xFF)
        hex[i.shl(1)] = HEX_CHARS[bv.ushr(4).and(0xF)]
        hex[i.shl(1) + 1] = HEX_CHARS[bv.and(0xF)]
    }
    return String(hex)
}

fun ByteArray.toHex() : String {
    return toList().toHex()
}

private fun hexCharIdx(c: Char) : Int {
    for (i in 0.until(16)) {
        if (c == HEX_CHARS[i]) {
            return i
        }
    }
    throw IllegalArgumentException("couldn't find $c in $HEX_CHARS")
}

fun String.hexToBytes() : ByteArray {
    if (this.length.and(1) != 0) {
        throw IllegalArgumentException("length of $this is not divisible by 2")
    }
    val b = ByteArray(this.length/2)
    val lc = this.toLowerCase()
    var j = 0;
    for (i in 0.until(lc.length).step(2)) {
        val idx = hexCharIdx(lc[i])
        val idx2 = hexCharIdx(lc[i+1])
        b[j] = idx.and(0x0F).shl(4).or(idx2.and(0x0F)).toByte()
        j++
    }
    return b
}

